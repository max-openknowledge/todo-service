package de.openknowledge.domain.todo;

import de.openknowledge.application.todo.State;
import de.openknowledge.infrastructure.domain.entity.AbstractEntity;
import de.openknowledge.infrastructure.domain.entity.Identifiable;
import de.openknowledge.infrastructure.infrastructure.persistence.EntityManagerProducer;
import de.openknowledge.infrastructure.stereotypes.Repository;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.transaction.Transactional;
import java.sql.Date;
import java.time.LocalDateTime;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

@RunWith(Arquillian.class)
public class TodoRepositoryIT {
  private static final Logger LOG = LoggerFactory.getLogger(TodoRepositoryIT.class);

  @Inject
  private TodoRepository repository;

  @Deployment
  public static Archive<?> createDeployment() {
    PomEquippedResolveStage pomFile = Maven
        .resolver()
        .loadPomFromFile("pom.xml");

    WebArchive webArchive = ShrinkWrap
        .create(WebArchive.class)
        .addAsLibraries(pomFile
            .resolve("org.assertj:assertj-core")
            .withTransitivity()
            .asFile())
        .addAsLibraries(pomFile
            .resolve("org.apache.commons:commons-lang3")
            .withTransitivity()
            .asFile())
        .addClasses(TodoRepository.class, Repository.class, EntityManagerProducer.class, Todo.class, TodoNotFoundException.class)
        .addClasses(Todo.class, State.class, Title.class, Description.class, TodoStatus.class, DueDate.class)
        .addClass(Date.class)
        .addClasses(AbstractEntity.class, Identifiable.class)
        .addAsResource("import.h2.sql")
        .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
        .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    LOG.info("Generated: {}", webArchive.toString(true));

    return webArchive;
  }

  @Test
  public void testFindAllPaginated() {
    List<Todo> todos = repository.findAllPaginated(State.ALL, 5, 0);
    assertThat(todos).hasSize(5);
  }

  @Test
  public void testFindUnfinishedPaginated() {
    List<Todo> todos = repository.findAllPaginated(State.UNFINISHED, 5, 0);
    assertThat(todos).hasSize(3);
  }

  @Test
  public void testFindById() {
    Todo todo = repository.findById(1);
    assertThat(todo
        .getTitle()
        .getValue()).isEqualTo("clean fridge1");
    assertThat(todo
        .getDescription()
        .getValue()).isEqualTo("Its a mess");
    assertThat(todo
        .getDueDate()
        .getValue()).isEqualTo("2015-12-01T08:00:00");
    assertThat(todo.getTodoStatus()).isEqualTo(TodoStatus.DONE);
  }

  @Test
  public void findByIdShouldFailForWrongId() {
    assertThatThrownBy(() -> {
      repository.findById(99);
    }).isInstanceOf(TodoNotFoundException.class);
  }

  @Test
  @Transactional
  public void testDeleteById() {
    Todo todo = repository.findById(1);
    Todo deletedTodo = repository.deleteById(todo.getId());

    assertThat(todo.getTitle()).isEqualTo(deletedTodo.getTitle());
    assertThat(todo.getDescription()).isEqualTo(deletedTodo.getDescription());
    assertThat(todo.getDueDate()).isEqualTo(deletedTodo.getDueDate());
    assertThat(todo.getTodoStatus()).isEqualTo(deletedTodo.getTodoStatus());
  }

  @Test
  public void testSave() {
    Todo todo = Todo
        .newBuilder()
        .withTitle(new Title("Todo"))
        .withDescription(new Description("Todo description"))
        .withDueDate(new DueDate(LocalDateTime.now()))
        .withTodoStatus(TodoStatus.DONE)
        .build();

    Todo newTodo = repository.save(todo);

    assertThat(newTodo.getTitle()).isEqualTo(todo.getTitle());
    assertThat(newTodo.getDescription()).isEqualTo(todo.getDescription());
    assertThat(newTodo.getTodoStatus()).isEqualTo(todo.getTodoStatus());

  }

  @Test
  public void testUpdate() {
    Todo todo = repository.findById(2);

    todo.setTitle(new Title("New Title"));
    todo.setDescription(new Description("New Description"));
    todo.setTodoStatus(TodoStatus.fromBoolean(true));
    todo.setDueDate(new DueDate(LocalDateTime.MIN));

    repository.updateTodo(todo);

    Todo updatedTodo = repository.findById(2);

    assertThat(todo.getTitle()).isEqualTo(updatedTodo.getTitle());
    assertThat(todo.getDescription()).isEqualTo(updatedTodo.getDescription());
    assertThat(todo.getTodoStatus()).isEqualTo(updatedTodo.getTodoStatus());
    assertThat(todo.getDueDate()).isEqualTo(updatedTodo.getDueDate());
  }

  @Test
  @Transactional
  public void deleteShouldFailForWrongId() {
    assertThatThrownBy(() -> {
      repository.deleteById(99);
    }).isInstanceOf(TodoNotFoundException.class);
  }
}
