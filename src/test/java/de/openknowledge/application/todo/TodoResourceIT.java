package de.openknowledge.application.todo;

import de.openknowledge.application.JaxRsActivator;
import de.openknowledge.domain.todo.Description;
import de.openknowledge.domain.todo.DueDate;
import de.openknowledge.domain.todo.Title;
import de.openknowledge.domain.todo.Todo;
import de.openknowledge.domain.todo.TodoNotFoundException;
import de.openknowledge.domain.todo.TodoRepository;
import de.openknowledge.domain.todo.TodoStatus;
import de.openknowledge.domain.todo.TodoValidationErrorPayload;
import de.openknowledge.infrastructure.infrastructure.persistence.EntityManagerProducer;
import de.openknowledge.infrastructure.rest.error.ConstraintViolationExceptionMapper;
import de.openknowledge.infrastructure.rest.error.NotFoundExceptionMapper;
import de.openknowledge.infrastructure.rest.error.ValidationErrorDTO;
import de.openknowledge.infrastructure.rest.error.ValidationErrorPayload;
import io.restassured.RestAssured;
import org.hamcrest.Matchers;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.container.test.api.RunAsClient;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.arquillian.test.api.ArquillianResource;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.jboss.shrinkwrap.resolver.api.maven.Maven;
import org.jboss.shrinkwrap.resolver.api.maven.PomEquippedResolveStage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Date;
import java.time.LocalDateTime;

@RunAsClient
@RunWith(Arquillian.class)
public class TodoResourceIT {
  private static final Logger LOG = LoggerFactory.getLogger(TodoResourceIT.class);

  @Deployment
  public static Archive<?> createDeployment() {
    PomEquippedResolveStage pomFile = Maven
        .resolver()
        .loadPomFromFile("pom.xml");

    WebArchive archive = ShrinkWrap
        .create(WebArchive.class)
        .addAsLibraries(pomFile
            .resolve("org.apache.commons:commons-lang3")
            .withTransitivity()
            .asFile())
        .addAsLibraries(pomFile
            .resolve("org.mockito:mockito-core")
            .withTransitivity()
            .asFile())
        .addClasses(TodoResource.class, JaxRsActivator.class, BaseTodoDTO.class, FullTodoDTO.class, State.class, ListTodoDTO.class, ConstraintValidator.class)
        .addClasses(TodoRepository.class, EntityManagerProducer.class, TodoNotFoundException.class, NotFoundExceptionMapper.class)
        .addClasses(ConstraintViolationExceptionMapper.class, ValidationErrorDTO.class, ValidationErrorPayload.class, TodoValidationErrorPayload.class)
        .addClasses(Todo.class, Title.class, Description.class, DueDate.class, TodoStatus.class, LocalDateTime.class)
        .addClass(Date.class)
        .addAsResource("import.h2.sql")
        .addAsResource("test-persistence.xml", "META-INF/persistence.xml")
        .addAsWebInfResource(EmptyAsset.INSTANCE, "beans.xml");

    LOG.debug(archive.toString(true));
    return archive;
  }

  @ArquillianResource
  private URL baseURI;

  @Test
  public void getTodo() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .when()
        .get(getUriWithTodoId(1))
        .then()
        .statusCode(200)
        .body("id", Matchers.equalTo(1))
        .body("title", Matchers.equalTo("clean fridge1"))
        .body("description", Matchers.equalTo("Its a mess"))
        .body("dueDate", Matchers.equalTo("2015-12-01T08:00:00"))
        .body("done", Matchers.is(true));
  }

  @Test
  public void getNotExistingTodo() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .when()
        .get(getUriWithTodoId(9999))
        .then()
        .statusCode(404);
  }

  @Test
  public void getTodoWithNegativTodoId() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .when()
        .get(getUriWithTodoId(-1))
        .then()
        .statusCode(400);
  }

  @Test
  public void testGetTodosLimit5() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 5, 0))
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(5))
        .body("[0].id", Matchers.equalTo(1))
        .body("[0].title", Matchers.equalTo("clean fridge1"))
        .body("[0].dueDate", Matchers.equalTo("2015-12-01T08:00:00"))
        .body("[0].done", Matchers.equalTo(true));
  }

  @Test
  public void testGetTodosLimit2() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 2, 0))
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .body("size()", Matchers.is(2))
        .body("[0].id", Matchers.equalTo(1))
        .body("[0].title", Matchers.equalTo("clean fridge1"))
        .body("[0].dueDate", Matchers.equalTo("2015-12-01T08:00:00"))
        .body("[0].done", Matchers.equalTo(true));
  }

  @Test
  public void testGetTodosOffset5() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 0, 5))
        .then()
        .statusCode(204);
  }

  @Test
  public void getTodosShouldFailForWrongState() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("some", 5, 0))
        .then()
        .statusCode(400);
  }

  @Test
  public void getTodosShouldSucceed() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 5, 0))
        .then()
        .statusCode(200);
  }

  @Test
  public void getTodosShouldReturn206() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("UNFINISHED", 5, 0))
        .then()
        .statusCode(206);
  }

  @Test
  public void getTodosShouldFailForNegativeLimit() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", -1, 0))
        .then()
        .statusCode(400);
  }

  @Test
  public void getTodosShouldFailForTooBigLimit() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 11, 0))
        .then()
        .statusCode(400);
  }

  @Test
  public void getTodosShouldFailForNegativeOffset() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 5, -1))
        .then()
        .statusCode(400);
  }

  @Test
  public void getTodosShouldFailForTooBigOffset() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .get(getListUri("ALL", 5, 101))
        .then()
        .statusCode(400);
  }

  @Test
  public void deleteByIdShouldSucceed() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .delete(getDeleteUri(6))
        .then()
        .statusCode(204);
  }

  @Test
  public void deleteByIdShouldFailForUnusedId() throws Exception {
    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .when()
        .delete(getDeleteUri(99))
        .then()
        .statusCode(404);
  }

  @Test
  public void updateTodo() throws Exception {
    BaseTodoDTO todo = new BaseTodoDTO();
    todo.setTitle("New title");
    todo.setDescription("New description");
    todo.setDone(true);
    todo.setDueDate(LocalDateTime
        .now()
        .toString());

    RestAssured
        .given()
        .contentType(MediaType.APPLICATION_JSON)
        .body(todo)
        .when()
        .put(getUriWithTodoId(1))
        .then()
        .statusCode(204);

    todo = new BaseTodoDTO();
    todo.setTitle("clean fridge1");
    todo.setDescription("Its a mess");
    todo.setDone(true);
    todo.setDueDate(LocalDateTime
        .of(2015, 12, 1, 8, 0, 0, 0)
        .toString());

    RestAssured
        .given()
        .contentType(MediaType.APPLICATION_JSON)
        .body(todo)
        .when()
        .put(getUriWithTodoId(1))
        .then()
        .statusCode(204);
  }

  @Test
  public void updateTodoWithTitleNull() throws Exception {
    BaseTodoDTO todo = new BaseTodoDTO();
    todo.setDescription("New description");
    todo.setDone(true);
    todo.setDueDate(LocalDateTime
        .now()
        .toString());

    RestAssured
        .given()
        .contentType(MediaType.APPLICATION_JSON)
        .body(todo)
        .when()
        .put(getUriWithTodoId(1))
        .then()
        .statusCode(400)
        .body("[0].code", Matchers.equalTo("TITLE_NULL"))
        .body("[0].message", Matchers.equalTo("title must not be null"));
  }

  @Test
  public void updateTodoWithEmptyTitle() throws Exception {
    BaseTodoDTO todo = new BaseTodoDTO();
    todo.setTitle("");
    todo.setDescription("New description");
    todo.setDone(true);
    todo.setDueDate(LocalDateTime
        .now()
        .toString());

    RestAssured
        .given()
        .contentType(MediaType.APPLICATION_JSON)
        .body(todo)
        .when()
        .put(getUriWithTodoId(1))
        .then()
        .statusCode(400)
        .body("[0].code", Matchers.equalTo("TITLE_SIZE"))
        .body("[0].message", Matchers.equalTo("title size must be between 1 and 30"));
  }

  @Test
  public void updateTodoWithDueDateNull() throws Exception {
    BaseTodoDTO todo = new BaseTodoDTO();
    todo.setTitle("New title");
    todo.setDescription("New description");
    todo.setDone(true);

    RestAssured
        .given()
        .contentType(MediaType.APPLICATION_JSON)
        .body(todo)
        .when()
        .put(getUriWithTodoId(1))
        .then()
        .statusCode(400)
        .body("[0].code", Matchers.equalTo("DUEDATE_NULL"))
        .body("[0].message", Matchers.equalTo("dueDate must not be null"));
  }

  @Test
  public void createTodoShouldSucceed() throws Exception {
    Todo todo = Todo
        .newBuilder()
        .withTitle(new Title("Todo"))
        .withDescription(new Description("Description"))
        .withDueDate(new DueDate(LocalDateTime.now()))
        .withTodoStatus(TodoStatus.DONE)
        .build();

    BaseTodoDTO baseTodo = new BaseTodoDTO(todo);

    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .body(baseTodo)
        .when()
        .post(getCreateUri())
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .statusCode(201);

  }

  @Test
  public void createTodoShouldFailForEmptyTitle() throws Exception {
    Todo todo = Todo
        .newBuilder()
        .withTitle(new Title(""))
        .withDescription(new Description("Description"))
        .withDueDate(new DueDate(LocalDateTime.now()))
        .withTodoStatus(TodoStatus.DONE)
        .build();

    BaseTodoDTO baseTodo = new BaseTodoDTO(todo);

    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .body(baseTodo)
        .when()
        .post(getCreateUri())
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .statusCode(400);
  }

  @Test
  public void createTodoShouldFailForLargeTitle() throws Exception {
    Todo todo = Todo
        .newBuilder()
        .withTitle(new Title("Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vestibulum massa risus, volutpat sed vehicula quis, accumsan nec mauris. Phasellus pretium vel orci tempus blandit. In id blandit massa. Praesent aliquet eleifend. "))
        .withDescription(new Description("Description"))
        .withDueDate(new DueDate(LocalDateTime.now()))
        .withTodoStatus(TodoStatus.DONE)
        .build();

    BaseTodoDTO baseTodo = new BaseTodoDTO(todo);

    RestAssured
        .given()
        .accept(MediaType.APPLICATION_JSON)
        .contentType(MediaType.APPLICATION_JSON)
        .body(baseTodo)
        .when()
        .post(getCreateUri())
        .then()
        .contentType(MediaType.APPLICATION_JSON)
        .statusCode(400);
  }

  private URI getUriWithTodoId(final int todoId) throws URISyntaxException {
    return UriBuilder
        .fromUri(baseURI.toURI())
        .path("api")
        .path("todos")
        .path(Integer.toString(todoId))
        .build();
  }

  private URI getListUri(String state, int limit, int offset) throws URISyntaxException {
    return UriBuilder
        .fromUri(baseURI.toURI())
        .path("api")
        .path("todos")
        .queryParam("state", state)
        .queryParam("limit", limit)
        .queryParam("offset", offset)
        .build();
  }

  private URI getDeleteUri(int id) throws URISyntaxException {
    return UriBuilder
        .fromUri(baseURI.toURI())
        .path("api")
        .path("todos")
        .path(Integer.toString(id))
        .build();
  }

  private URI getCreateUri() throws URISyntaxException {
    return UriBuilder
        .fromUri(baseURI.toURI())
        .path("api")
        .path("todos")
        .build();
  }
}
