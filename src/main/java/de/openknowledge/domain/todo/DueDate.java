package de.openknowledge.domain.todo;

import javax.persistence.Embeddable;
import java.time.LocalDateTime;

@Embeddable
public class DueDate {
    private LocalDateTime value;

    public DueDate() {
        //for JPA
    }
    public DueDate(LocalDateTime dueDate) {
        this.value = dueDate;
    }

    public DueDate(String dueDate){
        this.value = LocalDateTime.parse(dueDate);
    }

    public LocalDateTime getValue() {
        return value;
    }
}
