package de.openknowledge.domain.todo;

import javax.ws.rs.NotFoundException;

import java.io.Serializable;

import static org.apache.commons.lang3.Validate.notNull;

public class TodoNotFoundException extends NotFoundException {

  static final long serialVersionUID = 1L;

  private final int todoId;

  public TodoNotFoundException(final int todoId) {
    super();
    this.todoId = todoId;
  }

  public TodoNotFoundException(final int todoId, final Throwable cause) {
    super(cause);
    this.todoId = todoId;
  }

  @Override
  public String getMessage() {
    return String.format("Todo %s was not found", todoId);
  }

  public int getTodoId() {
    return todoId;
  }
}
