package de.openknowledge.domain.todo;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import static org.apache.commons.lang3.Validate.notNull;

@Entity
@Table(name = "TAB_TODO")
public class Todo {
  @Id
  @Column(name = "PK_TODO_ID")
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private Integer id;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "TODO_TITLE"))
  private Title title;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "TODO_DESC"))
  private Description description;

  @Embedded
  @AttributeOverride(name = "value", column = @Column(name = "TODO_DUE_DATE"))
  private DueDate dueDate;

  @Enumerated
  @Column(name = "TODO_STATUS")
  private TodoStatus todoStatus;

  public Integer getId() {
    return id;
  }

  public void setId(final Integer id) {
    this.id = id;
  }

  public Title getTitle() {
    return title;
  }

  public void setTitle(final Title title) {
    this.title = title;
  }

  public Description getDescription() {
    return description;
  }

  public void setDescription(final Description description) {
    this.description = description;
  }

  public DueDate getDueDate() {
    return dueDate;
  }

  public void setDueDate(final DueDate dueData) {
    this.dueDate = dueData;
  }

  public TodoStatus getTodoStatus() {
    return todoStatus;
  }

  public void setTodoStatus(final TodoStatus todoStatus) {
    this.todoStatus = todoStatus;
  }

  @Override
  public String toString() {
    return "Todo{" + "id=" + id + ", title='" + title + '\'' + ", description='" + description + '\'' + ", dueDate='" + dueDate + '\''
        + ", todoStatus=" + todoStatus + '}';
  }

  public static TodoBuilder newBuilder() {
    return new TodoBuilder();
  }

  public static class TodoBuilder {
    private Todo todo = new Todo();

    public TodoBuilder withId(final Integer id) {
      this.todo.id = notNull(id, "id must not be null");
      return this;
    }

    public TodoBuilder withTitle(final Title title) {
      this.todo.title = notNull(title, "title must not be null");
      return this;
    }

    public TodoBuilder withDescription(final Description description) {
      this.todo.description = notNull(description, "description must not be null");
      return this;
    }

    public TodoBuilder withDueDate(final DueDate dueDate) {
      this.todo.dueDate = notNull(dueDate, "dueDate must not be null");
      return this;
    }

    public TodoBuilder withTodoStatus(final TodoStatus todoStatus) {
      this.todo.todoStatus = notNull(todoStatus, "todoStatus must not be null");
      return this;
    }

    public Todo build() {
      return this.todo;
    }
  }
}
