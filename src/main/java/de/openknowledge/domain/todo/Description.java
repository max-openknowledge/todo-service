package de.openknowledge.domain.todo;

import javax.persistence.Embeddable;

@Embeddable
public class Description {
    private String value;

    public Description() {
        //for JPA
    }
    public Description(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
