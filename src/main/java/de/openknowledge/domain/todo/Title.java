package de.openknowledge.domain.todo;

import javax.persistence.Embeddable;

@Embeddable
public class Title {
    private String value;


    public Title() {
        //for JPA
    }
    public Title(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
