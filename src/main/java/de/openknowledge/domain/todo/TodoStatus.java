package de.openknowledge.domain.todo;

public enum TodoStatus {
  DONE, OPEN;

  public static TodoStatus fromBoolean(boolean status) {
    if (status) {
      return DONE;
    }
    return OPEN;
  }

  public boolean toBoolean() {
    switch (this) {
    case OPEN:
      return true;
    case DONE:
      return false;
    default:
      throw new IllegalArgumentException();
    }
  }
}
