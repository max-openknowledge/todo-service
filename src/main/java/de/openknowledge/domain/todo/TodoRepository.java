package de.openknowledge.domain.todo;

import de.openknowledge.application.todo.State;
import de.openknowledge.infrastructure.stereotypes.Repository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static org.apache.commons.lang3.Validate.notNull;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.EntityNotFoundException;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;

@Repository
public class TodoRepository {

  private static final Logger LOG = LoggerFactory.getLogger(TodoRepository.class);

  @Inject
  private EntityManager entityManager;

  public List<Todo> findAllPaginated(State state, int limit, int offset) {
    LOG.debug("Get ALL todos");

    CriteriaBuilder cb = entityManager.getCriteriaBuilder();
    CriteriaQuery<Todo> cq = cb.createQuery(Todo.class);

    Root<Todo> root = cq.from(Todo.class);
    cq.select(root);

    if (state == State.UNFINISHED) {
      cq.where(cb.equal(root.get("todoStatus"), 1));
    }

    TypedQuery<Todo> query = entityManager.createQuery(cq);
    query.setFirstResult(offset * limit);
    query.setMaxResults(limit);

    List<Todo> resultList = query.getResultList();

    LOG.debug("Found {} todos", resultList.size());

    return resultList;
  }

  public Todo findById(final int id){
    LOG.debug("Locating todo with id {}", id);

    Todo todo = entityManager.find(Todo.class, id);

    if (todo == null) {
      LOG.warn("Found no todo with id {}", id);
      throw new TodoNotFoundException(id);
    }

    return todo;
  }

  public Todo save(final Todo todo) {
    notNull(todo, "todo must not be null");

    LOG.debug("Create Todo {}", todo.toString());

    entityManager.persist(todo);

    return todo;
  }

  public Todo deleteById(final int id){
    try {
      LOG.debug("Delete Todo with id {}", id);

      Todo reference = entityManager.getReference(Todo.class, id);
      entityManager.remove(reference);

      LOG.debug("Deleted Todo {}", reference);
      return reference;
    } catch (EntityNotFoundException e) {
      LOG.warn("Found no Todo with id {}", id);
      throw new TodoNotFoundException(id);
    }
  }

  public Todo updateTodo(final Todo todo) {
    notNull(todo, "Todo must not be null");

    LOG.debug("Update todo with id {}", todo.getId());

    entityManager.merge(todo);
    return todo;
  }

}
