package de.openknowledge.application.version;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.util.Properties;

@Path("version")
@Produces(MediaType.APPLICATION_JSON)
public class VersionResource {

  @GET
  public Response getVersion() {
    try {
      Properties properties = new Properties();

      properties.load(this
          .getClass()
          .getClassLoader()
          .getResourceAsStream("version.properties"));
      return Response
          .status(Response.Status.OK)
          .entity(new Version(properties.getProperty("version")))
          .build();

    } catch (IOException i) {
      return Response
          .status(Response.Status.INTERNAL_SERVER_ERROR)
          .build();
    }

  }

  private class Version {
    private String version;

    public Version(final String version) {
      this.version = version;
    }

    public String getVersion() {
      return version;
    }

    public void setVersion(final String version) {
      this.version = version;
    }
  }
}
