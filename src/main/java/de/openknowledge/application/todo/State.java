package de.openknowledge.application.todo;

public enum State {
  ALL, UNFINISHED, INVALID;

  @Override
  public String toString() {
    return name().toLowerCase();
  }

}