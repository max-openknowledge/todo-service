package de.openknowledge.application.todo;

import de.openknowledge.domain.todo.Todo;
import de.openknowledge.domain.todo.TodoStatus;

import java.io.Serializable;
import java.time.LocalDateTime;

public class FullTodoDTO implements Serializable {
  static final long serialVersionUID = 2L;

  private Integer id;

  private String description;

  private String title;

  private boolean done;

  private LocalDateTime dueDate;

    public FullTodoDTO() {

    }


    public FullTodoDTO(final Todo todo) {
        this.id = todo.getId();
        this.description = todo.getDescription().getValue();
        this.title = todo.getTitle().getValue();
        this.dueDate = todo.getDueDate().getValue();
        this.done = todo.getTodoStatus() == TodoStatus.DONE;
    }

    public Integer getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public String getTitle() {
        return title;
    }

    public boolean isDone() {
        return done;
    }

    public LocalDateTime getDueDate() {
        return dueDate;
    }

    @Override
    public String toString() {
        return "FullTodo{" +
                "id=" + id +
                ", description='" + description + '\'' +
                ", title='" + title + '\'' +
                ", done=" + done +
                ", dueDate=" + dueDate +
                '}';
    }
}