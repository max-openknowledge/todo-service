package de.openknowledge.application.todo;

import de.openknowledge.domain.todo.Description;
import de.openknowledge.domain.todo.DueDate;
import de.openknowledge.domain.todo.Title;
import de.openknowledge.domain.todo.Todo;
import de.openknowledge.domain.todo.TodoNotFoundException;
import de.openknowledge.domain.todo.TodoRepository;
import de.openknowledge.domain.todo.TodoStatus;
import de.openknowledge.domain.todo.TodoValidationErrorPayload;
import de.openknowledge.infrastructure.rest.error.ValidationErrorDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.enums.ParameterIn;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.parameters.RequestBody;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.inject.Inject;
import javax.persistence.EntityExistsException;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.DefaultValue;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.List;
import java.util.stream.Collectors;

@Path("todos")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class TodoResource {
  private static final Logger LOG = LoggerFactory.getLogger(TodoResource.class);

  @Inject
  private TodoRepository repository;

  @GET
  @Path("{todoId}")
  @Operation(tags = { "Todos" }, description = "Request an existing todo", parameters = {
      @Parameter(in = ParameterIn.PATH, name = "todoId", description = "todoId - The todo identifier", required = true, schema = @Schema(type = "integer")) }, responses = {
      @ApiResponse(responseCode = "200", description = "Todo found", content = @Content(mediaType = "application/json", schema = @Schema(implementation = FullTodoDTO.class))),
      @ApiResponse(responseCode = "404", description = "Todo not found") })
  public Response getTodo(
      @PathParam("todoId") @Min(value = 0, payload = TodoValidationErrorPayload.NegativeTodoId.class, message = "NEGATIVE_TODO_ID") final Integer todoId) {
    LOG.debug("Request for todo with id {}", todoId);

    Todo persistedTodo = repository.findById(todoId);

    return Response.ok().entity(new FullTodoDTO(persistedTodo)).build();
  }

  @GET
  @Operation(description = "Get a list of todos.", tags = "Todos", parameters = {
      @Parameter(name = "state", description = "Filters ALL or UNFINISHED todos in the response", schema = @Schema(type = "string", defaultValue = "UNFINISHED", description = "Filters ALL or UNFINISHED todos in the response")),
      @Parameter(name = "limit", description = "Maximal number of todos in the response", schema = @Schema(type = "integer", defaultValue = "5", minLength = 0, maxLength = 10, description = "Maximal number of todos in the response")),
      @Parameter(name = "offset", description = "Offset for the todos in the response", schema = @Schema(type = "integer", description = "Offset for the todos in the response")) }, responses = {
      @ApiResponse(responseCode = "200", description = "List of todos.", content = {
          @Content(mediaType = "application/json", schema = @Schema(implementation = ListTodoDTO.class)) }),
      @ApiResponse(responseCode = "204", description = "Empty list of todos", content = { @Content(mediaType = "application/json") }),
      @ApiResponse(responseCode = "206", description = "Partial list of todos.", content = { @Content(mediaType = "application/json") }),
      @ApiResponse(responseCode = "400", description = "Invalid query params", content = { @Content(mediaType = "application/json") }) })
  public Response getTodos(@DefaultValue("UNFINISHED") @QueryParam("state") State state,
      @Min(value = 0, payload = TodoValidationErrorPayload.LimitMinInvalid.class) @Max(value = 10, payload = TodoValidationErrorPayload.LimitMaxInvalid.class) @DefaultValue("5") @QueryParam("limit") int limit,
      @Min(value = 0, payload = TodoValidationErrorPayload.OffsetMinInvalid.class) @Max(value = 100, payload = TodoValidationErrorPayload.OffsetMaxInvalid.class) @QueryParam("offset") int offset) {

    LOG.debug("Find all todos");

    List<ListTodoDTO> todos = repository.findAllPaginated(state, limit, offset)
        .stream()
        .map(ListTodoDTO::new)
        .collect(Collectors.toList());

    LOG.debug("Found {} todos", todos.size());

    if (todos.isEmpty()) {
      return Response.status(Response.Status.NO_CONTENT).build();
    }
    if (state.equals(State.UNFINISHED)) {
      return Response.status(Response.Status.PARTIAL_CONTENT).entity(todos).build();
    }
    return Response.status(Response.Status.OK).entity(todos).build();
  }

  @PUT
  @Path("{todoId}")
  @Transactional
  @Operation(description = "Update an existing todo.", tags = "Todos", parameters = {
      @Parameter(in = ParameterIn.PATH, name = "todoId", description = "todoId - The todo identifier", required = true, schema = @Schema(type = "integer")) }, requestBody = @RequestBody(description = "The modified todo", required = true, content = @Content(mediaType = "application/json", schema = @Schema(implementation = BaseTodoDTO.class))), responses = {
      @ApiResponse(responseCode = "204", description = "Todo updated"), @ApiResponse(responseCode = "404", description = "Todo not found"),
      @ApiResponse(responseCode = "400", description = "Invalid modified todo", content = { @Content(mediaType = "application/json") }) })
  public Response updateTodo(@PathParam("todoId") final Integer todoId, @Valid final BaseTodoDTO todo) {
    LOG.debug("Request to update todo with id {}" + todoId);

    Todo todoToUpdate;

    try {
      todoToUpdate = repository.findById(todoId);

      todoToUpdate.setTitle(new Title(todo.getTitle()));
      todoToUpdate.setDescription(new Description(todo.getDescription()));
      todoToUpdate.setDueDate(new DueDate(todo.getDueDate()));
      TodoStatus newStatus = todo.isDone() ? TodoStatus.DONE : TodoStatus.OPEN;
      todoToUpdate.setTodoStatus(newStatus);

      repository.updateTodo(todoToUpdate);
      LOG.debug("Updated todo with id {}", todoId);

    } catch (TodoNotFoundException e) {
      LOG.warn("Could not update todo with id {}", todoId);
      return Response.status(Response.Status.NOT_FOUND).build();
    }
    return Response.status(Response.Status.NO_CONTENT).build();
  }

  @DELETE
  @Path("{todoId}")
  @Transactional
  @Operation(description = "Delete an existing todo.", tags = "Todos", parameters = {
      @Parameter(in = ParameterIn.PATH, name = "todoId", required = true, schema = @Schema(type = "integer")) }, responses = {
      @ApiResponse(responseCode = "204", description = "Todo deleted."),
      @ApiResponse(responseCode = "404", description = "Todo not found.") })

  public Response deleteTodo(@PathParam("todoId") final int todoId) {
    LOG.debug("Delete Todo with id {}", todoId);

    repository.deleteById(todoId);

    return Response.status(Response.Status.NO_CONTENT).build();
  }

  @POST
  @Transactional
  @Operation(description = "Create a new todo.", tags = "Todos", parameters = {
      @Parameter(name = "body", description = "The new todo.", schema = @Schema(implementation = BaseTodoDTO.class)), }, responses = {
      @ApiResponse(responseCode = "201", description = "Todo created."),
      @ApiResponse(responseCode = "400", description = "Invalid new todo.", content = @Content(schema = @Schema(implementation = ValidationErrorDTO.class))) })
  public Response createTodo(@Valid BaseTodoDTO todo) {
    try {
      LOG.debug("Create new Todo");

      Todo newTodo = Todo.newBuilder()
          .withTitle(new Title(todo.getTitle()))
          .withDescription(new Description(todo.getDescription()))
          .withDueDate(new DueDate(todo.getDueDate()))
          .withTodoStatus(TodoStatus.fromBoolean(todo.isDone()))
          .build();

      repository.save(newTodo);
      LOG.info("Created new {}", todo);

      return Response.status(Response.Status.CREATED).entity(new FullTodoDTO(newTodo)).build();
    } catch (EntityExistsException e) {
      return Response.status(Response.Status.NO_CONTENT).build();
    }
  }
}
