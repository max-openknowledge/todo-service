package de.openknowledge.application.todo;

import de.openknowledge.domain.todo.Todo;
import de.openknowledge.domain.todo.TodoValidationErrorPayload;
import io.swagger.v3.oas.annotations.media.Schema;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.io.Serializable;

@Schema(name = "BaseTodo", description = "A base todo used for creation and modification.")
public class BaseTodoDTO implements Serializable {

  static final long serialVersionUID = 3L;

  @NotNull(payload = TodoValidationErrorPayload.TitleIsInvalid.class)
  @Size(min = 1, max = 30, payload = TodoValidationErrorPayload.TitleSize.class)
  private String title;

  @Size(min = 0, max = 500, payload = TodoValidationErrorPayload.DescriptionSize.class)
  private String description;

  @NotNull(payload = TodoValidationErrorPayload.DueDateNull.class)
  private String dueDate;

  private boolean done;

  public BaseTodoDTO() {
  }

  public BaseTodoDTO(Todo todo) {
    this.title = todo.getTitle().getValue();
    this.description = todo.getDescription().getValue();
    this.dueDate = todo.getDueDate().getValue().toString();
    this.done = todo.getTodoStatus().toBoolean();
  }

  public String getTitle() {
    return title;
  }

  public String getDescription() {
    return description;
  }

  public String getDueDate() {
    return dueDate;
  }

  public boolean isDone() {
    return done;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public void setDueDate(String dueDate) {
    this.dueDate = dueDate;
  }

  public void setDone(boolean done) {
    this.done = done;
  }

  @Override
  public String toString() {
    return "BaseTodoDTO{" + "title='" + title + '\'' + ", description='" + description + '\'' + ", dueDate='" + dueDate + '\'' + ", done="
        + done + '}';
  }
}
