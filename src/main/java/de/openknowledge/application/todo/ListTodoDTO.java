package de.openknowledge.application.todo;

import de.openknowledge.domain.todo.Todo;
import de.openknowledge.domain.todo.TodoStatus;
import io.swagger.v3.oas.annotations.media.Schema;

import java.time.LocalDateTime;

@Schema(name = "ListTodo", description = "A reduced todo with identifier used as response object for the todo list.")
public class ListTodoDTO {
  private int id;

  private String title;

  private LocalDateTime dueDate;

  private boolean done;

  public ListTodoDTO(Todo todo) {
    this.id = todo.getId();
    this.title = todo.getTitle().getValue();
    this.dueDate = todo.getDueDate().getValue();
    this.done = todo.getTodoStatus() == TodoStatus.DONE;
  }

  public int getId() {
    return id;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public LocalDateTime getDueDate() {
    return dueDate;
  }

  public void setDueDate(LocalDateTime dueDate) {
    this.dueDate = dueDate;
  }

  public boolean getDone() {
    return done;
  }

  public void setDone(boolean done) {
    this.done = done;
  }
}
