package de.openknowledge.infrastructure.rest.error;

import io.swagger.v3.oas.annotations.media.Schema;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintViolation;
import javax.validation.Payload;
import java.io.Serializable;

@Schema(name = "Error Response", description = "Error Response")
public class ValidationErrorDTO implements Serializable {

  private static final Logger LOG = LoggerFactory.getLogger(ValidationErrorDTO.class);

  static final long serialVersionUID = 4L;

  private String code;

  private String message;

  public ValidationErrorDTO() {
  }

  public ValidationErrorDTO(final ConstraintViolation violation) {
    Class<? extends Payload> clazz = (Class<? extends Payload>)violation.getConstraintDescriptor().getPayload().iterator().next();
    ValidationErrorPayload payload = null;
    try {
      payload = (ValidationErrorPayload)clazz.newInstance();
      this.code = payload.getErrorCode();
      this.message = payload.getMessage();
    } catch (InstantiationException | IllegalAccessException e) {
      LOG.error("Exception: {}", e);
    }
  }

  public String getCode() {
    return code;
  }

  public String getMessage() {
    return message;
  }
}
