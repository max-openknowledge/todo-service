package de.openknowledge.infrastructure.rest.error;

import de.openknowledge.domain.todo.TodoNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.ws.rs.NotFoundException;
import javax.ws.rs.core.Response;
import javax.ws.rs.ext.ExceptionMapper;
import javax.ws.rs.ext.Provider;

@Provider
public class NotFoundExceptionMapper implements ExceptionMapper<NotFoundException> {
    private static final Logger LOG = LoggerFactory.getLogger("NotFoundExceptionMapper.class");

    @Override
    public Response toResponse(NotFoundException e) {
        if (e instanceof TodoNotFoundException) {
            LOG.warn("Found no todo with id {}", ((TodoNotFoundException)e).getTodoId());
            return Response.status(Response.Status.NOT_FOUND).build();
        } else if (e.getCause() instanceof IllegalArgumentException) {
            LOG.error("Wrong input for QueryParam");
            return Response.status(Response.Status.BAD_REQUEST).build();
        } else {
            LOG.error("Some strange Exception");
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }
}
