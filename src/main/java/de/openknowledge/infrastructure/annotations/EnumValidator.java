package de.openknowledge.infrastructure.annotations;

import java.util.ArrayList;
import java.util.List;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import de.openknowledge.application.todo.State;

public class EnumValidator implements ConstraintValidator<ValidateEnum, State> {
  private List<String> valueList;

  private static final Logger LOG = LoggerFactory.getLogger(EnumValidator.class);

  @Override
  public void initialize(final ValidateEnum constraintAnnotation) {
    valueList = new ArrayList<String>();
    for (String val : constraintAnnotation.acceptedValues()) {
      valueList.add(val.toUpperCase());
    }
  }

  @Override
  public boolean isValid(final State value, final ConstraintValidatorContext context) {
    LOG.info("Validating value {} of queryParam state", value);
    return value != State.INVALID;
  }
}
