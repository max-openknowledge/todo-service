# 19-mwe-vbe-todo-service

Java EE Application written with JAX-RS and deployed with Wildfly 17.0.1 on Openshift 4.2 

## 📝 Table of Contents
- [Getting Started](#getting_started)
    - [Prerequisites](#getting_started_pre)
    - [Build](#getting_started_build)
    - [Test](#getting_started_test)
    - [OpenAPI Spec](#getting_started_openapi)
- [Infrastructure](#infra)
- [OpenShift](#openshift)


## 🏁 Getting Started  <a name = "getting_started"></a>

### Prerequisites  <a name = "getting_started_pre"></a>

You need the following software to run the app

- Maven
- Docker
- docker-compose

### Build <a name = "getting_started_build"></a>

To build the app run 
```sh
docker-compose up --build
```

### Test <a name = "getting_started_test"></a>

To run the unit tests run:
```sh
mvn test
```

and to run the integration tests run:
```sh
mvn verify -Dskip.surefire.tests=true
```
### Generate the OpenAPI Spec <a name = "getting_started_openapi"></a>

First you need to deploy the app via docker-compose.
Then navigate to `http://localhost:8181/19-mwe-vbe-todo-service/swagger-ui/index.html`

## 🚀 Infrastructure <a name = "infra"></a>

[Here](./docs/INFRASTRUCTURE.md) you can find a guide on how to setup a local infrastructure containing:
- GitLab
- Jenkins
- Nexus

## 🔮 OpenShift <a name = "openshift"></a>

### Prerequisites  <a name = "openshift_pre"></a>

You need a running OpenShift 4 cluster.
[Here](./docs/CRC.md) you find guide on how to install OpenShift 4 with CodeReady Containers VM on your Mac

### Projects

You need to create three projects. 
This first Project containes the infrastructure with Jenkins Gitlab, Nexus and SonarQube.
The second is the dev project, here we will deploy the dev/test version of the service.
The last project will contain the production version of the app.

```sh
oc create project 00-infra
```  
```sh
oc create project 01-dev
```  
```sh
oc create project 02-prod
```  

### Dev Environment

Navigate to the Web Console and set the Developer View.
Add a new _App_ from the _Catalog_ with the _JBoss EAP 7.2_ template.

- set the Application Name
- set the correct Git URL
- remove the context dir
- optional: set your Maven mirror URL

Next you need to set the Git credentials

#### Credentials

1. create a username/password secret with the same credentials form your Git Repository
1. Edit the BuildConfig for the app
    - edit your uri for the repository
    - edit the branch
    - add your secret name
    ```yaml
    source:
     type: Git
       git:
         uri: >-
           https://gitlab.openknowledge.de/ok/e-lab/studi/19-mwe-vbe-todo-service.git
         ref: ELAB-openshift
       sourceSecret:
         name: gitlab-creds
    ```
    
#### Jenkins ServiceAccount
Jenkins doesnt have the right privileges to start builds.
Run this to give the jenkins user the correct right
```sh
oc policy add-role-to-user edit system:serviceaccount:deployment:jenkins -n 01-dev
```