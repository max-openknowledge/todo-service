# Create a development enviroment on OpenShift

## Prerequisites  <a name = "prerequisites"></a>

You need a running OpenShift 4 cluster.
[Here](./docs/CRC.md) you find guide on how to install OpenShift 4 with CodeReady Containers VM on your Mac

## Install

Install all pods sequential and not parallel(it may break if you install all at once).

## Setup Namespaces

You need to create 3 namespaces.

- `00-infra` for the infrastructure. Here you install Jenkins and other apps like nexus, SonarQube or GitLab.
- `01-dev` this namespace is for dev/test purposes. The pipeline will build the dev version of the app.
- `02-prod` will contain the production version of the application

You can create a namespace in the Administrator View under _Administration > Namespace_

## Setup GitLab

Because GitLab runs as root you need to add root permissions for the default user:
`oc adm policy add-scc-to-user anyuid -z default`

Now you can Navigate to Add and select _Container Image_. Search for _gitlab/gitlab-ce_ and set the right _Application_.
Wait until Gitlab is done(it takes a while).

You can push the repo to:
`git remote add origin http://gitlab-deployment.apps-crc.testing/root/todo-service.git `

### Problems

If you cant navigate to Gitlab check if the route is correct: `oc get routes` 
if the port us 22 change in the _Admininistrator_>_Networkung_>_Routes_
and change the __targetPort__ to 80

## Setup Jenkins

Select the developer view.
Navigate to Add and select _From Catalog_.
Select _Jenkins_(not the Ephemeral), set the Memory limit to ~4Gi and instantiate the template.
Wait until Jenkins is done.

Create Pipeline and select _Pipeline_ > _Definition_ = Pipeline script from SCM and enter
`http://gitlab-deployment.apps-crc.testing/root/todo-service.git`.
Set the credentials as username/password from GitLab

## Setup Nexus

Navigate to Add and select _Container Image_. Search for _sonatype/nexus3_ and set the right _Application_.
Wait until Nexus is done.

Get the admin passrod by running:
` oc exec POD cat /nexus-data/admin.password`

Add the port that you selected for the docker registry in the nexus service.
Navigate to _Topology_ > _nexus3_ > _Services_ and select the Service. Next select the 
_YAML_ tab and add this to the ports field:
```yaml
- name: 9999-tcp
    protocol: TCP
    port: 9999
    targetPort: 9999
```



