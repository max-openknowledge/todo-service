# Install CoderReady Containers on your Mac


CodeReady Containers is a tool that manages a local OpenShift 4.x cluster optimized for testing and development purposes https://code-ready.github.io/crc/.
Its a VM were your own single node OpenShift 4 cluster resides.



## 🎈 Install

### Prerequisite

To download the binaries you need a Red Hat Account.
You can create yours [here](https://www.redhat.com/wapps/ugc/register.html;jsessionid=FHBPChI5pb030Q6YT7qnSASc.3839b12b?_flowId=register-flow&_flowExecutionKey=e1s1).

### 🛫 Get Started

Download the binary [here](https://cloud.redhat.com/openshift/install/crc/installer-provisioned) (you need your Red Hat Account), while you are there get your _pull secret_.
You will need it during the setup of the VM

To manage the cluster you need the CLI tool `oc`.
The easiest way to install it is via Brew.
```sh
brew update
brew install openshift-cli
```

After you downloaded the crc archive unpack it and move the binary to a safe location like _/usr/local/Cellar/crc_.
Try to start it. Mac will complain that it doesnt trust this developer.

To start it properly you have to goto _🍎 > System Preferences > Security & Privacy > 🔒_ and enable the _crc_ binary.

Move the binary to your `$PATH`. If the binary is in _/usr/local/Cellar/crc/crc_ you can link it like:
```sh
ln -s /usr/local/Cellar/crc/crc /usr/local/bin
```

Now you can start _crc_ like any other CLI tool.

### 🗂 Setup

Run:
```sh
crc setup
```
it will ask for the _pull secret_, after you added it you have to wait for a few minutes to setup the VM.

### 🏁 Start

You should increase the memory of the VM:
`oc config set memory 12000` increases the size to 12Gi.

Run:
```sh
crc start
```
after it is done (could take a few minutes) you are done. 

Run:
```sh
crc console
```
and login as admin. _crc_ will tell you the credentials.

Now your are done with the setup of the CodeReady Containers VM 🎉🎉🎉