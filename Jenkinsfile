def mvn(cmd) {
    configFileProvider(
            [configFile(fileId: 'deployment-settings', variable: 'MAVEN_SETTINGS')]) {
        sh "mvn -s $MAVEN_SETTINGS ${cmd}"
    }
}

def projectDev = '01-dev'
def projectProd = '02-prod'
def buildDev = 'bc/eap-app-dev'
def buildProd = 'bc/eap-app-prod'

pipeline {
    agent any

    tools {
        maven 'maven3.6.1'
    }

    stages {
        stage("clean") {
            steps {
                mvn 'clean'
            }
        }
        // run unit and integration test in parallel
        stage('Run Tests') {
            steps {
                parallel(
                        unitTest: {
                            mvn 'test'
                        }//,
                        //integrationTest: {
                        //    mvn 'verify -Dskip.surefire.tests=true'
                        //}
                )
            }
        }
        stage("SonarQube") {
            steps {
                mvn 'sonar:sonar'
            }
        }
        // deploy archetype to snapshot repository
        stage("Deploy SNAPSHOT") {
            steps {
                mvn 'deploy -Dmaven.test.skip=true'
            }
        }
        // trigger the application S2I build.
        stage('Trigger Development Build') {
            steps {
                script {
                    openshift.withCluster() {
                        openshift.withProject(projectDev) {
                            def buildConfig = openshift.selector(buildDev)
                            buildConfig.describe()
                            buildConfig.startBuild()
                        }
                    }
                }
            }
        }
        // ask the user if the app should be deployed
        stage('Promote to PRODUCTION?') {
            steps {
                timeout(time: 15, unit: 'MINUTES') {
                    input message: "Promote to PRODUCTION?", ok: "Promote"
                }
            }
        }
        //clean the release
        stage('Clean Release') {
            steps {
                mvn 'release:clean'
            }
        }
        // prepare the release
        stage('Prepare Deployment') {
            steps {
                sh 'git config --global user.email "jenkins@jenkins.com"'
                sh 'git config --global user.name jenkins'
                mvn 'release:prepare -Darguments="-DskipTests"'
            }
        }
        // deploy the release on nexus
        stage('Deploy Release') {
            steps {
                mvn 'release:perform'
            }
        }
        // trigger the production S2I build
        stage('Trigger Production Build') {
            steps {
                script {
                    openshift.withCluster() {
                        openshift.withProject(projectProd) {
                            def buildConfig = openshift.selector(buildProd)
                            buildConfig.describe()
                            buildConfig.startBuild()
                        }
                    }
                }
            }
        }
    }
}
